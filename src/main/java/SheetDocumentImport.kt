import com.google.gson.JsonObject
import org.apache.commons.io.FilenameUtils
import org.jetbrains.annotations.NotNull
import java.io.File
import java.io.IOException
import java.lang.Exception

class SheetDocumentImport (@NotNull val fileSheet: File) {



    private var fileFormat = SheetFileFormat.CSV_FILE_FORMAT


    enum class SheetFileFormat {
        CSV_FILE_FORMAT,
        XLS_FILE_FORMAT,
        XLSX_FILE_FORMAT,
        INVALID_FILE_FORMAT
    }


    init {
        println(FilenameUtils.getExtension(fileSheet.absolutePath))
        fileFormat = when(FilenameUtils.getExtension(fileSheet.absolutePath)) {
            CSV_EXTENSION -> SheetFileFormat.CSV_FILE_FORMAT
            XLS_EXTENSION -> SheetFileFormat.XLS_FILE_FORMAT
            XLSX_EXTENSION -> SheetFileFormat.XLSX_FILE_FORMAT
            else -> SheetFileFormat.INVALID_FILE_FORMAT
        }
    }


    fun getData(): JsonObject {
        if(!fileSheet.exists()) {
            throw IOException("El archivo no existe")
        }
        if(fileFormat == SheetFileFormat.INVALID_FILE_FORMAT) {
            throw FileNotCompatibleException("El archivo selecionado es invalido")
        } else {
            val document: DocumentImport = when(fileFormat) {
                SheetFileFormat.XLS_FILE_FORMAT -> XLSDocument(fileSheet)
                SheetFileFormat.CSV_FILE_FORMAT -> CSVDocument(fileSheet)
                else -> XLSDocument(fileSheet)
            }

        }
        return JsonObject()

    }


    inner class FileNotCompatibleException(msg: String) : Exception(msg)

    companion object {
        const val CSV_EXTENSION = "csv"
        const val XLS_EXTENSION = "xls"
        const val XLSX_EXTENSION = "xlsx"
        const val SHEET_DATA_POSITION = 0
        const val SHEED_TYPES_POSITION = 1
    }
}