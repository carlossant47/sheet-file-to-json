import com.google.gson.JsonObject
import org.apache.commons.lang.StringUtils
import org.apache.poi.hssf.usermodel.HSSFRow
import org.apache.poi.util.StringUtil
import java.io.File

abstract class DocumentImport(protected val file: File) {
    abstract var jsonData: JsonObject

    abstract fun readInformation()
    abstract var fieldsName: List<String>
    init {
        readInformation()
    }

    protected fun getHeaderNameField(input: HSSFRow): ArrayList<String> {
        val result = ArrayList<String>()
        for (cell in input) {
            val cellName = cell.stringCellValue
            val name = cellName
                    .replace("\\s".toRegex(),"")
                    .replace(cellName[0], cellName[0].toLowerCase())

            result.add(name)
        }
        return result
    }

    protected fun getHeaderNameField(input: Array<String>): ArrayList<String> {
        val result = ArrayList<String>()

        for (s in input) {
            val cellName = s
            val name = cellName
                    .replace("\\s".toRegex(),"")
                    .replace(cellName[0], cellName[0].toLowerCase())
            result.add(name)
        }
        return result
    }




}