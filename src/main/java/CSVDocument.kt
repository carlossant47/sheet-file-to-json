import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.opencsv.CSVReader
import java.io.File
import java.io.FileInputStream
import java.io.FileReader
import java.util.*
import kotlin.collections.ArrayList

class CSVDocument(file: File): DocumentImport(file) {

    override var fieldsName: List<String> = ArrayList()
    override fun readInformation() {
        val reader = CSVReader(FileReader(file))
        val r = reader.readAll()
        val jsonArray = JsonArray()
        r.forEachIndexed { index, strings ->
            if(index == 0) {
                fieldsName = getHeaderNameField(strings)
            }else {
                val jsonO = JsonObject()
                strings.forEachIndexed {i, s ->
                    jsonO.addProperty(fieldsName[i], s)
                }
                jsonArray.add(jsonO)
            }
        }
        println(jsonArray.toString())



    }

    override var jsonData: JsonObject = JsonObject()

}