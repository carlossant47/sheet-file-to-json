import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.CellType
import java.io.File
import java.io.FileInputStream
import org.apache.poi.ss.usermodel.FormulaEvaluator


class XLSDocument(file: File) : DocumentImport(file) {
    override var jsonData: JsonObject = JsonObject()

    override var fieldsName: List<String> = ArrayList()
    override fun readInformation() {
        val stream = FileInputStream(file)
        val wb = HSSFWorkbook(stream)
        val sheet = wb.getSheetAt(SheetDocumentImport.SHEET_DATA_POSITION)
        val formulaEvaluator = wb.creationHelper.createFormulaEvaluator()

        val jsonArray = JsonArray()

        //LEE PRIMERO LOS NOMBRES DE LOS CAMPOS
        fieldsName = getHeaderNameField(sheet.getRow(SheetDocumentImport.SHEET_DATA_POSITION))

        for (row in sheet) {
            if(row.rowNum != 0) {//IGNORA EL PRIMERA POSITION, DANDO A ENTENDER QUE SON LOS NOMBRES DE LOS CAMPOS
                val item = JsonObject()
                for (cell in row) {
                    val value = when(formulaEvaluator.evaluateInCell(cell).cellTypeEnum) {
                        CellType.NUMERIC -> cell.numericCellValue.toFloat()
                        else -> cell.stringCellValue
                    }.toString()
                    //AGREGA LOS VALORES EN UN JSON CON SU CORRESPONDIENTE VALOR
                    item.addProperty(fieldsName[cell.columnIndex], value)
                    jsonArray.add(item)
                }
            }
        }
        println(jsonArray.toString())
    }
}